function doIntro(){
	var tag1 = document.getElementById('tagline');
	var tl = new TimelineLite();
	tl.to($("#tag1"), 1, {css:{left: -20, scaleX: 0.7, scaleY:1.2}, ease: Back.easeOut, delay: 1.5});
	tl.to($("#tag1"), 0.6, {css:{left:0, scaleX:1, scaleY:1}, ease: Back.easeOut, delay: -0.5});

	tl.to($("#tag2"), 1, {css:{left: 306, scaleX: 0.7, scaleY:1.2}, ease: Back.easeOut, delay: 0});
	tl.to($("#tag2"), 0.6, {css:{left:326, scaleX:1, scaleY:1}, ease: Back.easeOut, delay: -0.5});

	tl.to($("#tag1"), 0.5, {css:{scale:1.1}, ease: Back.easeInOut, delay:-0.5})
		.to($("#tag1"), 0.5, {css:{scale:0.8}, ease: Back.easeInOut, delay: -0.3})
		.to($("#tag1"), 0.5, {css:{scale:1}, ease: Back.easeOut});

	tl.to($("#tag2"), 0.5, {css:{scale:1.1}, ease: Back.easeInOut, delay:-0.5})
		.to($("#tag2"), 0.5, {css:{scale:0.8}, ease: Back.easeInOut, delay:-0.3})
		.to($("#tag2"), 0.5, {css:{scale:1}, ease: Back.easeOut});

}

$(window).load(function(){
	doIntro();
    $("img.trigger").mouseover(function(){
        TweenLite.to($(this), 0.25, {css:{scale:1.05, top: "-2px"}, ease: Back.easeInOut});
    }).mouseout(function(){
        TweenLite.to($(this), 0.25, {css:{scale:1.0, top:0}, ease: Back.easeInOut});
    });
});