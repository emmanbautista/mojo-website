<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div id="container">
	<div id="bg-banner-sub">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline-sub-terms"><img src="images/termsconditions.png" alt="tagline" border="0" /></div>
        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  		<div id="content-area">
            	<div class="sub-contents">
                	<h1>Terms and Conditions</h1>
                    <div class="grayline"></div>
                    <p>Thank you for visiting our website. If  you want to use this website, you must agree to conform to and be legally bound  by the terms and conditions described below. <br />
                    <h3>IF YOU DISAGREE  WITH ANY OF THESE TERMS OR CONDITIONS, DO NOT USE OUR  WEBSITE.</h3><br />
                    <h3>1. MINORS.</h3>
                    <p>We do not provide  services or sell products to children. If you are below the age of 18, you may  use our website only with the permission and active involvement of a parent or  legal guardian. If you are a minor, please do not provide us or other website  visitors with any personal information. </p>
                    <h3>2. PRIVACY POLICY IS PART OF THESE  TERMS AND CONDITIONS.</h3>
                    <p>Our privacy policy is  part of, and subject to, these terms and conditions of use. You may view our  privacy policy on this website. </p>
                    <h3>3. ANTI-SPAM POLICY IS PART OF THESE  TERMS AND CONDITIONS.</h3>
                    <p>Our anti-Spam policy is  part of, and subject to, these terms and conditions of use. You may view our  anti-Spam policy on this website.</p>
                    <h3>4. MODIFICATIONS AND TERMINATIONS.</h3>
                    <p>These terms and conditions  may change from time to time. If such changes are made, they will be effective  immediately, and we will notify you by a notice posted on our website's home  page of the changes that have been made. If you disagree with the changes that  have been made, you should not use our website.</p>
                    <p>We may  terminate these terms and conditions of use for any reason and at any time  without notice to you.</p>
                    <p>If you are concerned  about these terms and conditions of use, you should read them each time before  you use our website. Any questions or concerns should be brought to our  attention by sending an e-mail to info@gentechpharma.com, and providing us with  information relating to your concern.</p>
                    <h3>5. LICENSEE  STATUS.</h3>
                    <p>You understand and agree  that your use of our website is limited and non-exclusive as a revocable  licensee. We may terminate your license to use our website, and access to our  website, for any reason, and without giving you notice. </p>
                    <h3>6. CONTENT  OWNERSHIP.</h3>
                    <p>All content on our  website is owned by us or our content suppliers. On behalf of ourselves and our  content suppliers, we claim all property rights, including intellectual  property rights, for this content and you are not allowed to infringe upon  those rights. We will prosecute to the fullest extent of the law anyone who attempts  to steal our property.</p>
                    <p>You agree not to copy  content from our website without our permission. Any requests to use our  content should be submitted to us by e-mail to info@gentechpharma.com.</p>
                    <p>If you believe that your  intellectual property rights have been infringed upon by our website content,  please notify us by sending an e-mail to info@gentechpharma.com,or by sending mail to us at the address listed below. Please  describe in detail the alleged infringement, including the factual and legal  basis for your claim of ownership.</p>
                    <h3>7.  DISCLAIMERS AND LIMITATIONS OF LIABILITY.</h3>
                    <p>The information on our website is  provided on an ''as is,'' ''as available'' basis. You agree that your use of  our website is at your sole risk. We disclaim all warranties of any kind,  including but not limited to, any express warranties, statutory warranties, and  any implied warranties of merchantability, fitness for a particular purpose,  and non-infringement. We do not warrant that our website will always be  available, access will be uninterrupted, be error-free, meet your requirements,  or that any defects in our website will be corrected.</p>
                    <p>Information  on our website should not necessarily be relied upon and should not to be  construed to be professional advice from us. We do not guarantee the accuracy  or completeness of any of the information provided, and are not responsible for  any loss resulting from your reliance on such information.</p>
                    <p>If your jurisdiction does not allow  limitations on warranties, this limitation may not apply to you. Your sole and  exclusive remedy relating to your use of the site shall be to discontinue using  the site.</p>
                    <p>Under no circumstances will we be  liable or responsible for any direct, indirect, incidental, consequential  (including damages from loss of business, lost profits, litigation, or the  like), special, exemplary, punitive, or other damages, under any legal theory,  arising out of or in any way relating to our website, your website use, or the  content, even if advised of the possibility of such damages.</p>
                    <p>Our total liability for any claim  arising out of or relating to our website shall not exceed one hundred ($100)  dollars and that amount shall be in lieu of all other remedies which you may  have against us or our affiliates. Any such claim shall be subject to  confidential binding arbitration as described later in these terms and  conditions of use.</p>
                    <h3>8. OBSCENE AND OFFENSIVE CONTENT.</h3>
                    <p>We are not  responsible for any obscene or offensive content that you receive or view from  others while using our website. However, if you do receive or view such  content, please contact us by e-mail to info@gentechpharma.com so that we can investigate the matter. Although we are not  obligated to do so, we reserve the right to monitor, investigate, and remove  obscene or offensive material posted to our website.</p>
                    <h3>9. INDEMNIFICATION.</h3>
                    <p>You  understand and agree that you will indemnify, defend and hold us and our  affiliates harmless from any liability, loss, claim and expense, including  reasonable attorney's fees, arising from your use of our website or your  violation of these terms and conditions.</p>
                    <h3>10.  COMPLIANCE WITH GOVERNING LAW AND DISPUTE RESOLUTION.</h3>
                    <p>You agree to  obey all applicable laws while using our website.</p>
                    <p>You agree  that the laws of FL govern these terms and conditions of use without regard to  conflicts of laws provisions.</p>
                    <p>You also agree that any dispute between you  and us, excluding any intellectual property right infringement claims we pursue  against you, shall be settled solely by confidential binding arbitration per  the American Arbitration Association commercial arbitration rules. All claims  must arbitrated on an individual basis, and cannot be consolidated in any arbitration  with any claim or controversy of anyone else. All arbitration must occur in Lee, FL, U.S.A.. Each party  shall bear one half of the arbitration fees and costs incurred, and each party  is responsible for its own lawyer fees.</p>
                    <h3>11. SEVERABILITY OF THESE TERMS AND CONDITIONS.</h3>
                    <p>If any part  of these terms and conditions of use are determined by a court of competent  jurisdiction to be invalid or unenforceable, that part shall be limited or  eliminated to the minimum extent necessary so that the remainder of these terms  and conditions are fully enforceable and legally binding.</p>
                    <h3>12. HOW TO  CONTACT US.</h3>
                    <p>Any questions or concerns about these terms and  conditions of use should be brought to our attention by  e-mail to info@gentechpharma.com, and providing us with information relating to your  concern.</p>
                    <p>You may also mail your  concerns to us at the following address:<br />
                          <b>Gentech Pharmaceutical</b><br />
                          43 Barkley Circle, Suite 203<br />
                          Fort Myers, FL 33907<br />
                          U.S.A.
                    </p>
                    <h3>13. ENTIRE AGREEMENT.</h3>
                    <p>These terms  and conditions, including the policies incorporated herein by express  reference, constitutes your entire agreement with us with respect to your use  of our website.</p>
                    <p>These terms  and conditions were last updated on 08-06-2012. </p>
                    <p>Copyright &copy;  2008 Law Office of Michael E. Young PLLC, and licensed  for use by the owner of this website at Gentech-Pharmaceutical.com. All Rights Reserved.  No  portion of this document may be copied or used by anyone other than the  licensee without the express written permission of the copyright owner. </p>

                </div>

        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
