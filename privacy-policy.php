<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div id="container">
	<div id="bg-banner-sub">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline-sub-privacy"><img src="images/privacypolicy.png" alt="tagline" border="0" /></div>
        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  		<div id="content-area">
            	<div class="sub-contents">
                	<h1>Privacy Policy</h1>
                    <div class="grayline"></div>
                    <h3>1. HOW WE PROTECT YOUR PRIVACY.</h3>
                    <p>This privacy policy tells you how we collect, use, and protect your personal information. By visiting our website, you accept and agree to the terms and conditions of this privacy policy. In particular, you consent to our collection and use of your personal information as described in this privacy policy.</p>
                    <h3>2. MINORS.</h3>
                <p>We do not provide  services or sell products to children. If you are below the age of 18, you may  use our website only with the permission and active involvement of a parent or  legal guardian. If you are a minor, please do not provide us or other website  visitors with any personal information. </p>
                <h3>3. POLICY IS  PART OF OUR TERMS AND CONDITIONS OF USE.</h3>
                <p>Our privacy policy is  part of, and subject to, our website's terms and conditions of use. You may  view these terms and conditions on our website.</p>
                <h3>4. THE TYPE  OF INFORMATION WE COLLECT FROM YOU.</h3>
                <p>Like most places on the  Internet, simply by visiting our website you automatically tell us certain  information. This includes basic information such as your IP address, when you  visited, the website from where you came prior to visiting us, the website  where you go when you leave our website, your computer's operating system, and  the type of web browser that you are using. Our website automatically records  this basic information about you.</p>
                <p>And like many other  websites, we may use cookies. In plain English, this means information that our  website's server transfers to your computer. This information can be used to  track your session on our website. Cookies may also be used to customize our  website content for you as an individual. If you are using one of the common  Internet web browsers, you can set up your browser to either let you know when  you receive a cookie or to deny cookie access to your computer.</p>
                <p>We may also collect any  data that you provide us by posting it at our website or by e-mail. You can  always choose not to provide us with information. However, if you do withhold  information, we may deny you access to some or all of our website's services  and features.</p>
                <p>Some transactions between  you and our website may involve payment by credit card, debit card, checks,  money orders, and/or third party online payment services. In such transactions,  we will collect information related to the transaction as part of the course of  doing business with you, including your billing address, telephone number, and  other information related to the transaction.</p>
                <h3>5. WHAT WE DO  WITH YOUR INFORMATION.</h3>
                <p>We use your information  to operate our website's business activities. For example, we may use this data  to contact you about changes to our website, new services, or special offers, resolve disputes, troubleshoot issues, and enforce our  website's terms and conditions.</p>
                <p>As a general rule, we  will not give your data to third parties without your permission. However,  there are some important exceptions to this rule that are described in the  following paragraphs.</p>
                <p>We may, in  our sole discretion, provide information about you to law enforcement or other  government officials for purposes of fraud investigations, alleged intellectual  property infringement, or any other suspected illegal activity or matters that  may expose us to legal liability.</p>
                <p>Although we  do not disclose individually identifiable information, we may disclose  aggregate data about our website's visitors to advertisers or other third  parties for marketing and promotional purposes.</p>
                <p>From time to  time, we may use third party suppliers to provide services on our website. If a  supplier wants to collect information about you, you will be notified. However,  you will never be required to give information to a third party supplier. We  restrict the way third party suppliers can use your information. They are not  allowed to sell or give your information to others.</p>
                <h3>6. USER NAMES  AND PASSWORDS.</h3>
                <p>Your access to parts of  our website may be protected by a user name and a password. Do not give  your password to anyone. If you enter a  section of our website that requires a password, you should log out when you  leave. As a safety precaution, you should also close out of your web browser  completely and re-open it before viewing other parts of the Internet.</p>
                <h3>7. YOUR USE  OF INFORMATION AND UNSOLICITED JUNK E-MAIL.</h3>
                <p>If you obtain personally  identifiable information about another website user, you are not allowed to  disclose this information to anyone else without the consent of the user and  our consent too.</p>
                <p>We hate junk e-mail (Spam). Information you obtain  from our website about us or other site users cannot be used by you or others  to send unsolicited commercial e-mail or to send unsolicited commercial  communications via our website's posting or other communication systems.</p>
                <h3>8.  YOUR VOLUNTARY DISCLOSURE OF INFORMATION TO THIRD PARTIES WHO ARE NOT OUR  SUPPLIERS</h3>
                <p>You may choose to provide personal information to  website visitors or other third parties who are not our suppliers. Please use  caution when doing so. The privacy policies and customs of these third parties  determine what is done with your information.</p>
                <h3>9. AUTORESPONDERS.</h3>
                <p>We may use autoresponders to communicate with you  by e-mail. To protect your privacy, we use a verified opt-in system for such  communications and you can always opt-out of such communications using the  links contained in each autoresponder message. If you have difficulties opting  out, you may contact us by sending an e-mail to info@gentechpharma.com,  or sending us mail to the address listed below.</p>
                <h3>10. POLICY  CHANGES.</h3>
                <p>The terms of this policy  may change from time to time. If such changes are made, we will notify you by a  notice posted on our website's home page of the changes that have been made. If  you disagree with the changes that have been made, please contact us (by  e-mail, using a website contact form, or in writing by mail), and any changes  made to this policy will not apply to information we have collected from you  prior to making the changes.</p>
                <p>If you are concerned  about the topic covered by this policy, you should read it each time before you  use our website. Any questions or concerns about this policy should be brought  to our attention by sending an e-mail to info@gentechpharma.com and providing us with information  relating to your concern.</p>
                <p>You may also mail your  concerns to us at the following address:<br />
                  <b>Gentech Pharmaceutical</b><br />
                  43 Barkley Circle, Suite 203<br />
                  Fort Myers, FL 33907<br />
                  U.S.A.</p>
                <h3>11. CALIFORNIA PRIVACY RIGHTS.</h3>
                <p>If you are a  California resident and our customer, Cal. Civ. Code &sect; 1798.83 permits you to  request certain information about our disclosure of personal information to  third parties for their direct marketing purposes. To request this information,  please send an e-mail to info@gentechpharma.com or write us at the following address:</p>
                <p><b>Gentech Pharmaceutical</b><br />
                  43 Barkley Circle, Suite 203<br />
                  Fort Myers, FL 33907<br />
                  U.S.A.</p>
                <p>This policy was last updated on 08-06-2012.</p>
                <p>Copyright &copy;  2008 <a href="http://www.mikeyounglaw.com/">Law Office of Michael E. Young PLLC </a>, and licensed  for use by the owner of this website at Gentech-Pharmaceutical.com. All Rights Reserved.  No  portion of this document may be copied or used by anyone other than the  licensee without the express written permission of the copyright owner. </p>

                </div>

        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
