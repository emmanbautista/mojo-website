<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<link href="animation.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script type="text/javascript" src="js/gs/plugins/CSSPlugin.min.js"></script>
<script type="text/javascript" src="js/gs/easing/EasePack.min.js"></script>
<script type="text/javascript" src="js/gs/TweenLite.min.js"></script>
<script type="text/javascript" src="js/gs/TweenMax.min.js"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/animation.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        

        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 0,
                        left: 150,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 0,
                        left: 150,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 0,
                        left: 150,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: 0,
                        left: 150,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div class="preload"></div>
<div id="container">
	<div id="bg-banner">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline">
            <div class="tag1" id="tag1"></div>
            <div class="tag2" id="tag2"></div>
            <!--<img src="images/tagline.png" alt="tagline" border="0" />-->
          </div>
            
          <div class="bubbleInfo">
          		<img class="trigger trigger1" src="images/bottle-mojoboost.png" border="0" />
                
            	<table id="dpop" class="popup">
                    <tbody><tr>
                        <td id="topleft" class="corner"></td>
                        <td class="top"></td>
                        <td id="topright" class="corner"></td>
                    </tr>
        
                    <tr>
                        <td class="left"></td>
                        <td>
                            <table class="popup-contents">
                                <tbody>
                                <tr>
                                    <th><b>Mojo Boost</b> is the #1 selling functional beverage with a powerful formulation of nitric oxide (how Viagra works) plus testosterone boosters – so you can <span>GO ALL DAY, GO ALL NIGHT.</span><br /><br /></th>
                                </tr>
                                <tr>
                                    <td><b>Mojo Boost</b> can be used as a shot or mixed with your favorite alcohol.</th>
                                </tr>
                                <tr>
                                    <td colspan="2"><a href="mojo-boost.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('more2','','images/btn-more2.png',1)"><img src="images/btn-more.png" name="more2" width="46" height="19" border="0" id="more2" /></a></td>
                                </tr>
                                </tbody>
                            </table>
        
                        </td>
                        <td class="right"></td>    
                    </tr>
        
                    <tr>
                        <td class="corner" id="bottomleft"></td>
                        <td class="bottom"></td>
                        <td id="bottomright" class="corner"></td>
                    </tr>
                </tbody>
            </table>
          </div>
          
          <div class="bubbleInfo2">
                <img class="trigger trigger2" src="images/bottle-mojochill.png" border="0" />
            	<table id="dpop2" class="popup">
                    <tbody><tr>
                        <td id="topleft" class="corner"></td>
                        <td class="top"></td>
                        <td id="topright" class="corner"></td>
                    </tr>
        
                    <tr>
                        <td class="left"></td>
                        <td>
                            <table class="popup-contents">
                                <tbody>
                                <tr>
                                    <th><b>Mojo Chill</b> is the world's first functional beverage designed for rapid stress Relief & Anxiety Relief.<br /><br /></th>
                                </tr>
                                <tr>
                                    <td><b>Mojo Chill</b> has been described as "Liquid Xanax", but with the complete ability to focus and enjoy your day!</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><a href="mojo-chill.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('more3','','images/btn-more2.png',1)"><img src="images/btn-more.png" name="more3" width="46" height="19" border="0" id="more3" /></a></td>
                                </tr>
                                </tbody>
                            </table>
        
                        </td>
                        <td class="right"></td>    
                    </tr>
        
                    <tr>
                        <td class="corner" id="bottomleft"></td>
                        <td class="bottom"></td>
                        <td id="bottomright" class="corner"></td>
                    </tr>
                </tbody>
            </table>
          </div>
          
          <div class="bubbleInfo3">
                <img class="trigger trigger3" src="images/bottle-mojofocus.png" border="0" />
            	<table id="dpop3" class="popup">
                    <tbody><tr>
                        <td id="topleft" class="corner"></td>
                        <td class="top"></td>
                        <td id="topright" class="corner"></td>
                    </tr>
        
                    <tr>
                        <td class="left"></td>
                        <td>
                            <table class="popup-contents">
                                <tbody>
                                <tr>
                                    <th><b>Mojo Focus</b> is more than just an energy drink – it is a mental enhancement functional beverage.  Be 100% at both work and school with the only drink designed to increase cognitive ability! </th>
                                </tr>
                                <tr>
                                    <td colspan="2"><a href="mojo-focus.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('more4','','images/btn-more2.png',1)"><img src="images/btn-more.png" name="more4" width="46" height="19" border="0" id="more4" /></a></td>
                                </tr>
                                </tbody>
                            </table>
        
                        </td>
                        <td class="right"></td>    
                    </tr>
        
                    <tr>
                        <td class="corner" id="bottomleft"></td>
                        <td class="bottom"></td>
                        <td id="bottomright" class="corner"></td>
                    </tr>
                </tbody>
            </table>
          </div>
          
          <div class="bubbleInfo4">
                <img class="trigger trigger4" src="images/bottle-mojo1.5.png" border="0" />
            	<table id="dpop4" class="popup">
                    <tbody><tr>
                        <td id="topleft" class="corner"></td>
                        <td class="top"></td>
                        <td id="topright" class="corner"></td>
                    </tr>
        
                    <tr>
                        <td class="left"></td>
                        <td>
                            <table class="popup-contents">
                                <tbody>
                                <tr>
                                    <th>The finest bars from Miami & Manila to London & Hong Kong mix <b>Mojo Boost</b> into the best tasting drinks to provide their customers with amazing energy plus the confidence they can <span>GO ALL DAY, GO ALL NIGHT</span>!   The all-new 1.75 liter bottle is perfect for bars, clubs and large private parties.</th>
                                </tr>
                                <tr>
                                    <td colspan="2"><a href="mojo-boost.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('more5','','images/btn-more2.png',1)"><img src="images/btn-more.png" name="more5" width="46" height="19" border="0" id="more5" /></a></td>
                                </tr>
                                </tbody>
                            </table>
        
                        </td>
                        <td class="right"></td>    
                    </tr>
        
                    <tr>
                        <td class="corner" id="bottomleft"></td>
                        <td class="bottom"></td>
                        <td id="bottomright" class="corner"></td>
                    </tr>
                </tbody>
            </table>
          </div>

        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  <div id="content-area">
    		<div id="content-area-a">
            	<div class="content-col-text">
                	<h1>Videos</h1>
                  	<div class="grayline"></div>
                    <object width="290" height="162">
                    <param name="movie" value="http://www.youtube.com/v/Vmybsg1OYTQ?version=3&amp;hl=en_US&amp;rel=0">
                    <param name="allowFullScreen" value="true">
                    <param name="allowscriptaccess" value="always"><embed type="application/x-shockwave-flash" width="290" height="162" src="http://www.youtube.com/v/Vmybsg1OYTQ?version=3&amp;hl=en_US&amp;rel=0&amp;showinfo=0" allowscriptaccess="always" allowfullscreen="true">
                    </object>
                </div>
            </div>
            
            <div id="content-area-b">
           	  <div class="content-col-text">
                	<h1>Where to buy</h1>
                	<div class="grayline"></div>
                  	<img src="images/img-wheretobuy.png" alt="bottles" border="0" align="left" style="margin:10px 20px 20px 0px;"/>
                	<p>Looking for your favorite Mojo Beverage? Click here for local and international locations near you.</p>
                <a href="where-to-buy.php" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('more','','images/btn-more3-hover.png',1)"><img src="images/btn-more3.png" name="more" width="62" height="26" border="0" id="more" /></a></div>
            </div>
            
            <div id="content-area-c">
           	  <div class="content-col-text">
                	<h1>Contact</h1>
                	<div class="grayline"></div>
                  	<img src="images/img-distributor.png" alt="video" border="0" align="right" style="margin:10px 0px 20px 0px;" />
                    <p><b>Connect with us</b></p>
                    <a href="https://www.facebook.com/MojoPacific" target="_blank"><img src="images/icon-fb.png" alt="fb" border="0" style="margin:0px 5px 5px 0px;" /></a>
                   
                    <a href="http://www.youtube.com/mojoboost" target="_blank"><img src="images/icon-youtube.png" alt="myspace" border="0" style="margin:0px 5px 5px 0px;" /></a>
                    <p><b>Mojo Beverage International</b><br />
					43 Barkley Circle  #203 <br />
					Fort Myers, Fl 33907 <br />
					<b>Toll Free:</b> 888-666-1714 <br />
					<b>Philippines Direct:</b><br /> 0916-7961108</p>
            	</div>
            </div>
        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
