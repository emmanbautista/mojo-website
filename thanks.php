<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script type="text/javascript">

function checkFields() {
missinginfo = "";
if (document.contact_us.name.value == "") {
missinginfo += "\n     -  Name";
}
if ((document.contact_us.email.value == "") || 
(document.contact_us.email.value.indexOf('@') == -1) || 
(document.contact_us.email.value.indexOf('.') == -1)) {
missinginfo += "\n     -  Email address";
}

if(document.contact_us.message.value == "") {
missinginfo += "\n     -  Message";
}

if (missinginfo != "") {
missinginfo ="_____________________________\n" +
"You failed to correctly fill in your:\n" +
missinginfo + "\n_____________________________" +
"\nPlease re-enter and submit again!";
alert(missinginfo);
return false;
}
else return true;
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div id="container">
	<div id="bg-banner-sub">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline-sub-contact"><img src="images/contact.png" alt="tagline" border="0" /></div>
        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  		<div id="content-area">
    		<div id="content-left">
            	<div class="sub-contents">
                	<h1>Customer Service</h1>
                    <div class="grayline"></div>
                    <p>Thank you for your feedback! We will respond within 24 hours. Thank you!</p>
                    
                </div>
            </div>
            
            <div id="content-right">
            	<div class="sub-contents">
                	<h1>Video</h1>
                    <div class="grayline"></div>
                    <object width="250" height="162">
                    <param name="movie" value="http://www.youtube.com/v/Vmybsg1OYTQ?version=3&amp;hl=en_US&amp;rel=0">
                    <param name="allowFullScreen" value="true">
                    <param name="allowscriptaccess" value="always"><embed type="application/x-shockwave-flash" width="250" height="162" src="http://www.youtube.com/v/Vmybsg1OYTQ?version=3&amp;hl=en_US&amp;rel=0&amp;showinfo=0" allowscriptaccess="always" allowfullscreen="true">
                    </object>
                </div>
                
                <div class="sub-contents">
                	<h1>Contact</h1>
                    <div class="grayline"></div>
                    <p><b>Connect with us</b></p>
                    <a href="https://www.facebook.com/MojoPacific" target="_blank"><img src="images/icon-fb.png" alt="fb" border="0" style="margin:0px 5px 5px 0px;" /></a>
                    
                    <a href="http://www.youtube.com/mojoboost" target="_blank"><img src="images/icon-youtube.png" alt="myspace" border="0" style="margin:0px 5px 5px 0px;" /></a>
                    <p><b>Mojo Beverage International</b><br />
					43 Barkley Circle #203 <br /> Fort Myers, Fl 33907 <br />
					<b>Toll Free:</b> 888-666-1714 <br />
					<b>Philippines Direct:</b><br /> 0916-7961108</p>
                </div>
            </div>
            
            <div class="clearboth"></div>
        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
