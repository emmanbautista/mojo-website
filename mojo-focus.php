<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div id="container">
	<div id="bg-banner-sub">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline-sub"><img src="images/ourbeverages.png" alt="tagline" border="0" /></div>
        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  		<div id="content-area">
            	<div class="sub-contents">
                	<h1>Mojo Focus</h1>
                    <div class="grayline"></div>
                  	<img src="images/sub-mojofocus.png" alt="mojoboost" border="0" align="left" style="margin:0px 30px 0px 0px;" />
           	    	<h2>Mojo Focus - The "Liquid Adderall Alternative" for Millions</h2>
                    <h3>Available in select American cities and universities only.</h3>
                    <p>The absolute need for enhanced mental performance in today's academic and business society is undisputed. Scholastic requirements are as intense as the most competitive corporate environments and the world is simply demanding more and higher levels of performance.</p>
                    <p>In very much the same way steroids have been used to increase physical performance in both collegiate and professional sports – specific medications such as Adderall have been used to enhance cognitive abilities in a significant manner. The allure of 4.0 vs a 2.5 GPA or a CEO position vs mid-management is too important of a reality to ignore or dismiss.</p>
                    <p>Mojo Focus is more than just an energy drink – it is a mental enhancement functional beverage.  Mojo Focus allows you to be 100% at both work and school with the only drink designed to increase cognitive ability!   Even better - Mojo Focus is a safe alternative for millions of Adderall users without ADD!</p>
                    <p>Once you try Mojo Focus you will never again want to take on a hard day at school or work without the extra focus and ability this little 2.2oz functional beverage provides!</p>
                    <h2>Benefits and Brain Health - Ampheta-CDP (Mojo Focus Active Components) </h2>
                  	<ul>
                    	<li>Improve Memory and Learning</li>
                        <li>Positive Mood</li>
                        <li>Enhanced Cognitive Ability</li>
                        <li>Improves Brain Function</li>
                        <li>Reduces Anxiety</li>
                    </ul>
                    <div class="clearboth"></div>
                    <table cellpadding="0" cellspacing="0" border="0" width="960" class="table-comparison">
                    	<tr>
                        	<td rowspan="9" width="250"><img src="images/img-focus.jpg" border="0" alt="focus" /></td>
                            <td style="background:#7ea0b5;"><h1>COMPARISON CATEGORIES</h1></td>
                            <td style="background:#7ea0b5;"><h1>MOJO FOCUS</h1></td>
                            <td style="background:#7ea0b5;"><h1>ADDERALL™</h1></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Prescription Strength</p></td>
                            <td><p>YES</p></td>
                            <td><p>YES</p></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Prolonged energy</p></td>
                            <td><p>YES</p></td>
                            <td><p>YES</p></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Crosses blood brain barrier</p></td>
                            <td><p>YES</p></td>
                            <td><p>YES</p></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Designed for non-ADD/ADHD</p></td>
                            <td><p>YES</p></td>
                            <td><span>NO</span></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Memory Support</p></td>
                            <td><p>YES</p></td>
                            <td><span>NO</span></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Side Effects</p></td>
                            <td><p>Minimal</p></td>
                            <td><span>Strong</span></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Requires Prescription</p></td>
                            <td><p>NO</p></td>
                            <td><span>YES</span></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Safe long-term use</p></td>
                            <td><p>YES</p></td>
                            <td><span>Ask your Physician</span></td>
                        </tr>
                    </table>
                    <p>Now available in the U.S. in certain locations.</p>
        	</div>

        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
