<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<title>Mojo Beverage International Website</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<script src="js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo2').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo3').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
<script type="text/javascript">
    <!--

    $(function () {
        $('.bubbleInfo4').each(function () {
            var distance = 10;
            var time = 250;
            var hideDelay = 30;

            var hideDelayTimer = null;

            var beingShown = false;
            var shown = false;
            var trigger = $('.trigger', this);
            var info = $('.popup', this).css('opacity', 0);


            $([trigger.get(0), info.get(0)]).mouseover(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                if (beingShown || shown) {
                    // don't trigger the animation again
                    return;
                } else {
                    // reset position of info box
                    beingShown = true;

                    info.css({
                        top: -90,
                        left: -33,
                        display: 'block'
                    }).animate({
                        top: '-=' + distance + 'px',
                        opacity: 1
                    }, time, 'swing', function() {
                        beingShown = false;
                        shown = true;
                    });
                }

                return false;
            }).mouseout(function () {
                if (hideDelayTimer) clearTimeout(hideDelayTimer);
                hideDelayTimer = setTimeout(function () {
                    hideDelayTimer = null;
                    info.animate({
                        top: '-=' + distance + 'px',
                        opacity: 0
                    }, time, 'swing', function () {
                        shown = false;
                        info.css('display', 'none');
                    });

                }, hideDelay);

                return false;
            });
        });
    });
    
    //-->
</script>
</head>

<body onload="MM_preloadImages('images/btn-more.png','images/btn-more2.png')">
<div id="container">
	<div id="bg-banner-sub">
    	<div id="bg-menu">
            <div id="top-container">
                <div id="top-left">
                    <div id="logo"><a href="index.php"><img src="images/logo.png" alt="logo" border="0" /></a></div>
                </div> <!-- end of top left -->
                
                <div id="top-right">
                    <nav>
                        <ul>
                            <li><a href="who-we-are.php">Who we are</a></li>
                            <li><a href="#">Our Beverages</a>
                                <ul>
                                    <li><a href="mojo-boost.php">Mojo Boost</a></li>
                                    <li><a href="mojo-chill.php">Mojo Chill</a></li>
                                    <li><a href="mojo-focus.php">Mojo Focus</a></li>
                                </ul>
                            </li>
                            <li><a href="distributors.php">Distributors</a></li>
                            <li><a href="faqs.php">FAQS</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                    </nav>
                </div> <!-- end of top right -->
                
                <div class="clearboth"></div>
            </div> <!-- end of top cont -->
        </div> <!-- end of bg menu -->
        
        <div class="clearboth"></div>
        
        <div id="banner">
       	  <div id="tagline-sub"><img src="images/ourbeverages.png" alt="tagline" border="0" /></div>
        </div> <!-- end of banner -->
        
        <div class="clearboth"></div>
        
  </div> <!-- end of bg banner -->
    
  <div class="clearboth"></div>
    
    <div id="bg-content">
  		<div id="content-area">
            	<div class="sub-contents">
                	<h1>Mojo Boost</h1>
                    <div class="grayline"></div>
                  	<img src="images/sub-mojoboost.png" alt="mojoboost" border="0" align="left" style="margin:0px 30px 0px 0px;" />
                    <h2>What is MOJO BOOST™?</h2>
                    <p>MOJO BOOST™ is the #1 selling designer functional beverage that provides hours of physical enhancement for men and women. It is formulated with a powerful proprietary formula of nitric oxide (how Viagra TM works) and testosterone boosters propelled by energy enhancers, vitamins, amino acids and clinically proven non-prescription blends.</p>
                    <h2>The "Viagra Connection"</h2>
                  	<p>Viagra and MOJO BOOST™ both use the very specific function of Nitric Oxide to enable their products to provide the desired results. Viagra is a prescription pharmaceutical while MOJO BOOST™ is a Non-Prescription Pharmaceutical designer beverage. MOJO BOOST™ contains a prescription component, but due to our proprietary formulation we are licensed to sell MOJO BOOST™ without a prescription!</p>
                    <p>MOJO BOOST™ and Viagra may have some common elements, but it is the differences that make MOJO BOOST™ such a phenomenal product.</p>
                    <ul>
                    	<li>MOJO BOOST™ is first and foremost the world class leader for designer functional beverages providing your body with an amazing formula for consistent high octane performance without letdown or crash. You will not find a better way to increase and maintain your energy and stamina regardless of the activity – no amount of Viagra will do that!</li>
                        <li>MOJO BOOST's™ positive effect on the endocrine system greatly improves libido and thus sexual stimulation.</li>
                        <li>MOJO BOOST™ increases mental alertness and focus for sustained enjoyment of activities.</li>
                        <li>MOJO BOOST™ enhancement formula provides Complete Confidence – and that is the difference between a normal night and a MOJO night!</li>
                    </ul>
                    <div class="clearboth"></div>
                    <table cellpadding="0" cellspacing="0" border="0" width="960" class="table-comparison">
                    	<tr>
                        	<td rowspan="9" width="250"><img src="images/img-girl.jpg" border="0" alt="focus" /></td>
                            <td width="200" style="background:#7ea0b5;"><h1>INGREDIENT BENEFIT</h1></td>
                            <td style="background:#7ea0b5;"><h1>EFFECT ON SEXUAL PERFORMANCE</h1></td>
                            <td width="80" style="background:#7ea0b5;"><h1>VIAGRA</h1></td>
                            <td width="80" style="background:#7ea0b5;"><h1>MOJO BOOST™</h1></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Improved Usage of Nitric Oxide</p></td>
                            <td><p>A key chemical required to produce a large erection and increase blood flow to the penis.</p></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Increase Testosterone</p></td>
                            <td><p>Required for a healthy libido, strong performance, faster recovery, greater strength and more energy.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Increase Energy</p></td>
                            <td><p>Increased energy levels, increased performance and stamina. No Energy Level = No Performance.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Increase Circulatory Health</p></td>
                            <td><p>Required for strong blood flow.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Increase Libido</p></td>
                            <td><p>Increased sex drive and desire. No sex drive = no sex. Healthy sex drive = peak performance capabilities.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Calms and Sharpens the Mind</p></td>
                            <td><p>Reduce performance stress and anxiety.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce0e3;">
                        	<td><p>Anti oxidant</p></td>
                            <td><p>Protects the body providing more energy and stamina.</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                        <tr style="background:#dce6ed;">
                        	<td><p>Protects against diabetes & hypertension</p></td>
                            <td><p>These common medical conditions are often the cause of impotence, erectile dysfunction and weak performance</p></td>
                            <td></td>
                            <td><img src="images/icon-check.png" alt="check" border="0" style="margin:0px 20px 0px 30px;"/></td>
                        </tr>
                    </table>
                    
                    <div class="clearboth"></div>
                    
                    <img src="images/img-lab.jpg" align="right" border="0" alt="lab" style="margin:0px 0px 0px 10px" />
                    <h2>The "Hormone Boost Connection" – Getting your MOJO back! - Males</h2>
                    <p>Testosterone is the primary hormone controlling sex drive and sexual performance for both men and women. If you want to improve your Sexual Health, then you need to increase your testosterone production – even if in minimal doses for short periods of time.</p>
                    <p>Testosterone is recognized as the hormone of desire: it makes muscles for boys and turns them into sexually functional men. Testosterone is widely recognized as the hormone that enables men to perform at peak physical levels both in general physical terms as well as sexually. It is also well documented that testosterone levels peak for males in their late teens or very early 20's – thus people often have the "wish" to have the sex drive of a teenager (GO ALL DAY, GO ALL NIGHT). MOJO BOOST™ is the only designer beverage in the world to directly, yet safely provide this essential testosterone boost. Increased testosterone provides strength, stamina, energy and enhanced performance!</p>
					<h2>MOJO BOOST™ 101</h2>
                    <p>MOJO BOOST™ was scientifically formulated in an advanced collaboration with the world's leading Non-Prescription Pharmaceutical manufacturer. This multi-year collaboration of Research and Development has produced the most effective functional beverage ever. MOJO BOOST™ is clinically designed to enhance both physical and mental performance which provides benefits never seen before in an energy beverage.</p>
                    <p>Down a MOJO BOOST™ in seconds and feel and see the MOJO difference for hours! GO ALL DAY, GO ALL NIGHT is more than just a slogan – it's a reality with MOJO BOOST™. Find out what DJs, stars, athletes and active people already know all around the globe – Nothing, even with a prescription, will give you the hardcore results of MOJO BOOST™.</p>
                    <h2>MOJO BOOST™ Drink Mixes</h2>
                    <table cellpadding="0" cellspacing="0" border="0" width="960" class="drink-mixes">
                    	<tr>
                          <td width="243"><p><b>SoCo Mojo</b> <br />Southern Comfort, Lime & Mojo Boost</p></td>	
                          <td width="243"><p><b>Jack Bomb</b> <br />Jack Daniels & Mojo Boost</p></td>
                          <td width="243"><p><b>Mojo Sex Shooter</b> <br />Tequila & Mojo Boost</p></td>
                          <td rowspan="8" width="229"><img src="images/img-drink.jpg" border="0" alt="focus" /></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Absolute Mojo Sex</b> <br />Absolute Vodka & Mojo Boost</p></td>	
                          <td width="243"><p><b>Mojo Three Way</b> <br />Vodka, Blue Curacao, Grenadine & Mojo Boost</p></td>
                          <td width="243"><p><b>Mojo Indulgence Martini</b> <br />Caramel and Chocolate on glass, Stoli Vanilla, Godiva White, Godiva Dark & Mojo Boost</p></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Happy Endings</b> <br />Blue Vodka, Lemonade & Mojo Boost</p></td>	
                          <td width="243"><p><b>Screaming Mojorgasm</b> <br />Orange Juice, Raspberry Vodka, Triple Sec Mojo Boost</p></td>
                          <td width="243"><p><b>Mojotini</b> <br />Grey Goose & Mojo Boost</p></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Mojo Bomb</b> <br />Jaeger & Mojo Boost</p></td>	
                          <td width="243"><p><b>Sex with the bartender</b> <br />Flavored Rum, Orange Juice, Strawberry Schnapps & Mojo Boost</p></td>
                          <td width="243"><p><b>Strawberry Mojotini</b> <br />3 Lemon Chunks, 3 Lime Chunks, Fist of Mint, 1 Strawberry</p></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Mojo Cherry Popper</b> <br />Cherry Vodka & Mojo Boost</p></td>	
                          <td width="243"><p><b>Mojo Margarita</b> <br />Mix Mojo Boost in your favorite Margarita</p></td>
                          <td width="243"><p><b>Bacardi Dragon Berry</b> <br />1/2 Mojo Boost & top with 7-up</p></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Mojo Foreplay</b> <br />Jaeger, Cinnamon Schnapps & Mojo Boost</p></td>	
                          <td width="243"><p><b>Mojo Panty Dropper</b> <br />Rum, Mojito Mix, Lime & Mojo Boost</p></td>
                          <td width="243"></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Mojo Sunset</b> <br />Vodka, Orange Juice & Mojo Boost</p></td>	
                          <td width="243"><p><b>Mojo Pink Passion</b> <br />Vodka, Peach Schnapps, Pineapple & Mojo Boost</p></td>
                          <td width="243"></td>
                        </tr>
                        <tr>
                          <td width="243"><p><b>Mojo Three Way</b> <br />Vodka, Blue Curacao, Grenadine & Mojo Boost</p></td>	
                          <td width="243"><p><b>Mojo Sex on the Beach</b> <br />Vodka, Cranberry, Peach Schnapps, Pineapple & Mojo Boost</p></td>
                          <td width="243"></td>
                        </tr>
                    </table>
        	</div>

        </div> <!-- end of content area -->
    </div> <!-- end of bg content -->
    
    <div class="clearboth"></div>
    
    <div id="bg-footer">
    	<div id="footer">
        	<div id="footer-left">
            	<img src="images/footer-logo.png" alt="footerlogo" border="0" align="left" style="margin:20px 20px 0px 0px;" />
            </div> <!-- end of footer left -->
            
            <div id="footer-right">
            	<div id="footer-links">
            		<a href="terms-conditions.php">TERMS AND CONDITIONS</a><span>l</span><a href="privacy-policy.php">PRIVACY POLICY</a>
                	<p>Copyright 2013. Mojo Beverage International. All Rights Reserved.</p>
                </div> <!-- end of footer links -->
            </div> <!-- end of footer right -->
            
            <div class="clearboth"></div>
        </div> <!-- end of footer -->
    </div> <!-- end of bg footer -->
    
    <div class="clearboth"></div>
    
</div> <!-- end of container -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5669059-1");
pageTracker._trackPageview();
</script>
</body>
</html>
